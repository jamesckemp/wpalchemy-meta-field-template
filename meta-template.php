<div class="my_meta_control">
 
	<?php
		$adminMetaboxes = array(
			array(
				"name" => "Internal ID",
				"label" => "internal_id",
				"type" => "text",
				"description" => "This is the description of the field"
			),
			array(
				"name" => "Title",
				"label" => "title",
				"type" => "select",
				"description" => "",
				"options" => array(
					"Prof" => "Prof",
					"Mr" => "Mr",
					"Mrs" => "Mrs",
					"Ms" => "Ms"
				)
			),
			array(
				"name" => "Forename",
				"label" => "forename",
				"type" => "text",
				"description" => ""
			)
		);
	?>
	
	<?php foreach($adminMetaboxes as $adminMetabox){ ?>
		
		<div class="metaField">
		
			<label><?php echo $adminMetabox['name']; ?></label>
			
			<?php $mb->the_field($adminMetabox['label']); ?>
			
			<?php if($adminMetabox['type'] == "text"){ ?>	 
				<input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/>
			<?php } ?>
			
			<?php if($adminMetabox['type'] == "select"){ ?>
				<select name="<?php $mb->the_name(); ?>">
					<option value="">Select...</option>
					<?php foreach($adminMetabox['options'] as $value => $label) { ?>
						<option value="<?php echo $value; ?>"<?php $mb->the_select_state($value); ?>><?php echo $label; ?></option>
					<?php } ?>
				</select>
			<?php } ?>
			
			<?php if($adminMetabox['description'] != "") { ?><span><?php echo $adminMetabox['description']; ?></span><?php } ?>
			
		</div>
	
	<?php } ?>

</div>